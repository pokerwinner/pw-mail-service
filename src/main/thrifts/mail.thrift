namespace java com.tiantian.mail.thrift.mail

const string version = '1.0.1'
enum Status{
    OK = 0,     //用户调用ok
    WARNING_SQL_NULL_QUERY = 500,
    ERROR_SQL_SYNTAX = 501,
    ERROR_SQL_FETCH_RESULT = 502,
    ERROR_SQL_INSERT_RESULT = 503,
    ERROR_SQL_UPDATE_RESULT = 504,
    ERROR_SQL_DELETE_RESULT = 505,
    ERROR_INIT = 999
}
struct MailReward {
    1:string mailRewardId,
    2:string mailId,
    3:string rewardCode,
    4:string rewardName,
    5:i64    rewardNumber
}

struct Mail {
    1:i32       status,
    2:string    errMsg,
    3:string    objId,
    4:string    fromUserId,
    5:string    toUserId,
    6:string    type,
    7:string    title,
    8:string    content,
    9:list<string>  referIds,
    10:i32       isRead,
    11:i32      isGet,
    12:i64      createDate,
    13:string   dealStatus,
    14:list<MailReward> mailRewardList
}

struct Notice {
    1:string userId,
    2:string content,
    3:string date
}


service MailService {
    string getServiceVersion(),
    Mail addMail(1:string fromUserId, 2:string toUserId, 3:string type, 4:string title, 5:string content,6:list<string> referIds,
                7:list<MailReward> rewardList),
    list<Mail> getMails(1:string userId, 2:i32 beginCnt, 4:i32 count),
    bool deleteMail(1:string mailId),
    bool updateReadMail(1:string objId),
    bool updateGetMail(1:string objId),
    bool updateDealStatusMail(1:string objId, 2:string status),
    Mail addSystemMessageMail(1:string toUserId, 2:string title, 3:string content),
    bool saveNotice(1:string userId, 2:string content),
    list<Notice> getNotice(1:string userId, 2:i32 beginCnt, 3:i32 count)
}