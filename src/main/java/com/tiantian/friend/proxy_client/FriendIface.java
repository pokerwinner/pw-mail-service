package com.tiantian.friend.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.friend.settings.FriendConfig;
import com.tiantian.friend.thrift.friend.FriendService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;

/**
 *
 */
public class FriendIface extends IFace<FriendService.Iface>{
    private static class FriendIfaceHolder {
        private final static FriendIface instance = new FriendIface();
    }

    private FriendIface() {
    }

    public static FriendIface instance() {
        return FriendIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new FriendService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(FriendConfig.getInstance().getHost(),
                FriendConfig.getInstance().getPort());
    }

    @Override
    protected Class<FriendService.Iface> faceClass() {
        return FriendService.Iface.class;
    }
}
