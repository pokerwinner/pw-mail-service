package com.tiantian.mail.client;

import com.tiantian.mail.settings.MailConfig;
import com.tiantian.mail.thrift.mail.MailService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 *
 */
public class MailClient {
    private TTransport transport;
    private MailService.Client client;

    public MailService.Client getClient() {
        return client;
    }

    public void setClient(MailService.Client client) {
        this.client = client;
    }

    public MailClient() {
        try {
            transport = new TSocket(MailConfig.getInstance().getHost(),
                    MailConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol(fastTransport);

            client = new MailService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }

    public void close() {
        if (null != transport) {
            transport.close();
            client = null;
        }
    }
}
