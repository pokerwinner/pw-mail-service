package com.tiantian.mail.handler;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.friend.proxy_client.FriendIface;
import com.tiantian.mail.data.mongodb.MGDatabase;
import com.tiantian.mail.entity.MailEntity;
import com.tiantian.mail.entity.MailRewardEntity;
import com.tiantian.mail.thrift.mail.*;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class MailHandler implements MailService.Iface {
    private static final String MAILS_TABLE = "mails";
    private static final String NOTICE_TABLE = "notices";
    @Override
    public String getServiceVersion() throws TException {
        return mailConstants.version;
    }

    @Override
    public Mail addMail(String fromUserId, String toUserId, String type, String title, String content,
                        List<String> referIds, List<MailReward> rewardList) throws TException {
        long createDate = System.currentTimeMillis();
        Document document = new Document();
        ObjectId objId = new ObjectId();
        document.put("_id", objId);
        document.put("fromUserId", fromUserId);
        document.put("toUserId", toUserId);
        document.put("type", type);
        document.put("title", title);
        document.put("content", content);
        document.put("referIds", referIds);
        document.put("isRead", 0);
        document.put("isGet", 0);
        document.put("isDelete", 0);
        document.put("createDate", createDate);
        document.put("dealStatus", "");
        List<Document> rewardDocs = new ArrayList<>();
        List<MailReward> mailRewardList = new ArrayList<>();
        if (rewardList != null && rewardList.size() > 0) {
            for (MailReward mailReward : rewardList) {
                 Document $document = new Document();
                 String mailRewardId = UUID.randomUUID().toString().replace("-", "");
                 mailReward.setMailRewardId(mailRewardId);
                 $document.put("mailRewardId", mailRewardId);
                 $document.put("mailId", objId.toString());
                 $document.put("rewardCode", mailReward.getRewardCode());
                 $document.put("rewardName", mailReward.getRewardName());
                 $document.put("rewardNumber", mailReward.getRewardNumber());
                 $document.put("createDate", createDate);
                 rewardDocs.add($document);
                 mailRewardList.add(mailReward);
            }
        }
        document.put("mailRewardList", rewardDocs);
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);
        mailsCollection.insertOne(document);

        Mail mail = new Mail(Status.OK.getValue(), null, objId.toString(), fromUserId, toUserId, type, title, content,referIds,
                0, 0, createDate, null, mailRewardList);
        return mail;
    }

    @Override
    public List<Mail> getMails(String userId, int beginCnt, int count) throws TException {

        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection("mails");
        FindIterable<Document> limitIter = mailsCollection.find(new BasicDBObject("toUserId", userId)
                .append("isDelete", 0).append("createDate", new BasicDBObject("$gt", System.currentTimeMillis() - 7 * 24 * 3600000))
        ).sort(new BasicDBObject("isRead", 1)
                .append("createDate", -1))
                .skip(beginCnt).limit(count);
        List<Mail> mailList = new ArrayList<>();
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            ObjectId $objId = doc.getObjectId("_id");
            String fromUserId = doc.getString("fromUserId");
            String toUserId = doc.getString("toUserId");
            String type = doc.getString("type");
            String title = doc.getString("title");
            String content = doc.getString("content");
            List<String> referIds = doc.get("referIds", List.class);
            Integer isRead = doc.getInteger("isRead");
            Integer isGet = doc.getInteger("isGet");
            Long createDate = doc.getLong("createDate");
            String dealStatus = doc.getString("dealStatus");
            List<Document> rewardDocs = doc.get("mailRewardList", List.class);
            List<MailReward> mailRewardList = new ArrayList<>();
            if (rewardDocs != null && rewardDocs.size() > 0) {
                for (Document document : rewardDocs) {
                     String mailRewardId = document.getString("mailRewardId");
                     String $mailId = document.getString("mailId");
                     String rewardCode = document.getString("rewardCode");
                     String rewardName = document.getString("rewardName");
                     Long rewardNumber = document.getLong("rewardNumber");
                     MailReward mailReward = new MailReward(mailRewardId, $mailId, rewardCode, rewardName, rewardNumber);
                     mailRewardList.add(mailReward);
                }
            }
            Mail mail = new Mail(Status.OK.getValue(), null, $objId.toString(), fromUserId, toUserId, type,
                    title, content, referIds ,isRead, isGet, createDate, dealStatus, mailRewardList);
            mailList.add(mail);
        }
        return mailList;
    }

    @Override
    public boolean deleteMail(String objId) throws TException {
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(objId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isDelete",1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);

        UpdateResult result = mailsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    @Override
    public boolean updateReadMail(String objId) throws TException {
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(objId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isRead",1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);

        UpdateResult result = mailsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    @Override
    public boolean updateGetMail(String objId) throws TException {
        MongoCollection<Document> friendsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(objId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isGet", 1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);

        UpdateResult result = friendsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }

    @Override
    public boolean updateDealStatusMail(String objId, String status) throws TException {
        MailEntity mailEntity = getMailByMailId(objId);
        if (mailEntity == null) {
            return false;
        }
        // 已经处理过了
        if (StringUtils.isNotBlank(mailEntity.getDealStatus())) {
            return false;
        }

        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(objId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("dealStatus", status);
        updatedValue.put("isRead",1);
        updatedValue.put("isGet", 1);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = mailsCollection.updateOne(updateCondition, updateSetValue);
        if (result.getModifiedCount() > 0){
            return switchType(mailEntity.getType(), mailEntity);
        } else {
            return false;
        }

    }

    private boolean switchType(String type, MailEntity mailEntity) {
        boolean result = false;
        switch (type) {
            case "1" : break;
            case "2" : break;
            case "3" : break;
            case "4" :
            // 好友申请
            List<String> referIds =  mailEntity.getReferIds();
            if (referIds != null && referIds.size() > 0) {
                try {
                    FriendIface.instance().iface().agreeApplication(referIds.get(0));
                } catch (TException e) {
                    e.printStackTrace();
                }
                // 不处理信息
                return true;
            }
            break;
            case "5" :
                break;
            default:break;
        }
        return result;
    }

    private MailEntity getMailByMailId(String objId) {
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(MAILS_TABLE);
        FindIterable<Document> limitIter = mailsCollection.find(new BasicDBObject("_id", new ObjectId(objId))).limit(1);
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        MailEntity mailEntity = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            mailEntity = new MailEntity();
            mailEntity.setObjId(objId);
            String fromUserId = doc.getString("fromUserId");
            mailEntity.setFromUserId(fromUserId);
            String toUserId = doc.getString("toUserId");
            mailEntity.setToUserId(toUserId);
            String type = doc.getString("type");
            mailEntity.setType(type);
            String title = doc.getString("title");
            mailEntity.setTitle(title);
            String content = doc.getString("content");
            mailEntity.setContent(content);
            List<String> referIds = doc.get("referIds", List.class);
            mailEntity.setReferIds(referIds);
            Integer isRead = doc.getInteger("isRead");
            mailEntity.setIsRead(isRead);
            Integer isGet = doc.getInteger("isGet");
            mailEntity.setIsGet(isGet);
            Long createDate = doc.getLong("createDate");
            mailEntity.setCreateDate(createDate);
            List<Document> rewardDocs = doc.get("mailRewardList", List.class);

            if (rewardDocs != null && rewardDocs.size() > 0) {
                List<MailRewardEntity> mailRewardEntities = new ArrayList<>();
                for (Document document : rewardDocs) {
                    MailRewardEntity mailRewardEntity = new MailRewardEntity();
                    String mailRewardId = document.getString("mailRewardId");
                    String $mailId = document.getString("mailId");
                    String rewardCode = document.getString("rewardCode");
                    String rewardName = document.getString("rewardName");
                    Long rewardNumber = document.getLong("rewardNumber");
                    mailRewardEntity.setMailRewardId(mailRewardId);
                    mailRewardEntity.setMailId($mailId);
                    mailRewardEntity.setRewardCode(rewardCode);
                    mailRewardEntity.setRewardName(rewardName);
                    mailRewardEntity.setRewardNumber(rewardNumber);
                    mailRewardEntities.add(mailRewardEntity);
                }
                mailEntity.setMailRewardList(mailRewardEntities);
            }
        }
        return mailEntity;
    }

    public Mail addSystemMessageMail(String toUserId, String title, String content) throws TException{
       return addMail("", toUserId, "1", title, content, null, null);
    }

    public boolean saveNotice(String userId, String content) {
        long createDate = System.currentTimeMillis();
        Document document = new Document();
        ObjectId objId = new ObjectId();
        document.put("_id", objId);
        document.put("userId", userId);
        document.put("content", content);
        document.put("createDate", createDate);
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(NOTICE_TABLE);
        mailsCollection.insertOne(document);
        return true;
    }

    public List<Notice> getNotice(String userId, int beginCnt, int count) {
        if (beginCnt > 200) {
            return new ArrayList<>(); // 最多200条
        }
        MongoCollection<Document> mailsCollection = MGDatabase.getInstance().getDB().getCollection(NOTICE_TABLE);
        FindIterable<Document> limitIter
                = mailsCollection.find(new BasicDBObject("userId", userId)
                .append("createDate", new BasicDBObject("$gt", System.currentTimeMillis() - 7 * 24 * 3600000)))
                .sort(new BasicDBObject("createDate", -1))
                .skip(beginCnt).limit(count);
        List<Notice> noticeList = new ArrayList<>();
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            ObjectId $objId = doc.getObjectId("_id");
            String $userId = doc.getString("userId");
            String content = doc.getString("content");
            Long createDate = doc.getLong("createDate");
            Notice notice = new Notice();
            notice.setContent(content);
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            notice.setDate(sf.format(new Date(createDate)));
            noticeList.add(notice);
        }
        return noticeList;
    }
}
