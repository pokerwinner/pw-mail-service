package com.tiantian.mail.server;

import com.tiantian.mail.data.mongodb.MGDatabase;
import com.tiantian.mail.handler.MailHandler;
import com.tiantian.mail.settings.MailConfig;
import com.tiantian.mail.thrift.mail.MailService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MailServer {
    private Logger LOG = LoggerFactory.getLogger(MailServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        MailServer groupServer = new MailServer();
        groupServer.init(args);
        groupServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            MGDatabase.getInstance().init();

            MailHandler handler = new MailHandler();
            TProcessor tProcessor = new MailService.Processor<MailService.Iface>(handler);
            TServerSocket tss = new TServerSocket(MailConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", MailConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }

}
