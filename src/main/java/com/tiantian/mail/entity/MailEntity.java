package com.tiantian.mail.entity;

import java.util.List;

/**
 * 邮件系统
 */
public class MailEntity {
    private String objId;
    // 邮件来自谁
    private String fromUserId;
    // 邮件发给谁
    private String toUserId;
    // 1:系统通知邮件, 2:系统奖励邮件, 3 私人赠与邮件, 4 好友申请邮件
    private String type;
    // 标题
    private String title;
    // 内容
    private String content;
    // 引用ID (比如公告的ID)
    private List<String> referIds;
    // 是否已经阅读 1是, 0 否
    private int isRead;
    // 是否领取 1 是, 0 否
    private int isGet;
    // 是否删除 1是 0否
    private int isDelete;
    private long createDate;
    // 处理状态  ok 表示已处理
    private String dealStatus;

    private List<MailRewardEntity> mailRewardList;

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getReferIds() {
        return referIds;
    }

    public void setReferIds(List<String> referIds) {
        this.referIds = referIds;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsGet() {
        return isGet;
    }

    public void setIsGet(int isGet) {
        this.isGet = isGet;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public List<MailRewardEntity> getMailRewardList() {
        return mailRewardList;
    }

    public void setMailRewardList(List<MailRewardEntity> mailRewardList) {
        this.mailRewardList = mailRewardList;
    }

    public String getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(String dealStatus) {
        this.dealStatus = dealStatus;
    }
}
