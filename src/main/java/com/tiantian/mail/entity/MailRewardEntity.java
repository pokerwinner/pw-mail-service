package com.tiantian.mail.entity;

/**
 * 邮件奖励信息
 */
public class MailRewardEntity {
    private String mailRewardId;
    //  邮件的ID
    private String mailId;
    // 奖励的ID
    private String rewardCode;
    // 奖励的名称
    private String rewardName;
    // 奖励的数量
    private long rewardNumber;
    // 创建时间
    private long createDate;

    public String getMailRewardId() {
        return mailRewardId;
    }

    public void setMailRewardId(String mailRewardId) {
        this.mailRewardId = mailRewardId;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public long getRewardNumber() {
        return rewardNumber;
    }

    public void setRewardNumber(long rewardNumber) {
        this.rewardNumber = rewardNumber;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }
}
