package com.tiantian.mail.utils;

import com.tiantian.mail.client.MailClient;
import com.tiantian.mail.client.MailFactory;
import com.tiantian.mail.settings.MailConfig;
import com.tiantian.mail.thrift.mail.Mail;
import com.tiantian.mail.thrift.mail.MailReward;
import com.tiantian.mail.thrift.mail.Notice;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.List;

/**
 *
 */
public class MailUtils {
    private ObjectPool<MailClient> pool;

    private MailUtils() {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(MailConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(MailConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<MailClient>(new MailFactory(), config);
    }

    private static class  MailUtilsHolder {
        private final static MailUtils instance = new MailUtils();
    }

    public static MailUtils getInstance() {
        return MailUtilsHolder.instance;
    }

    public String getServiceVersion() {
        MailClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version = client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public List<Mail> getMails(String userId, int beginCnt, int count){
        MailClient client = null;
        List<Mail> mailList = null;
        try {
            client = pool.borrowObject();
            mailList = client.getClient().getMails(userId, beginCnt, count);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mailList;
    }

    public Mail addMail(String fromUserId, String toUserId, String type, String title, String content,
                        List<String> referIds, List<MailReward> rewardList){
        MailClient client = null;
        Mail mail = null;
        try {
            client = pool.borrowObject();
            mail = client.getClient().addMail( fromUserId, toUserId, type, title,
                        content, referIds, rewardList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mail;
    }
    public boolean deleteMail(String mailId){
        MailClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().deleteMail(mailId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean updateGetMail(String mailId){
        MailClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().updateGetMail(mailId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean updateReadMail(String mailId){
        MailClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().updateReadMail(mailId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean updateDealStatusMail (String mailId, String status) {
        MailClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().updateDealStatusMail(mailId, status);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;

    }

    public Mail addSystemMessageMail(String toUserId, String title, String content){
        MailClient client = null;
        Mail mail = null;
        try {
            client = pool.borrowObject();
            mail = client.getClient().addSystemMessageMail(toUserId, title,
                    content);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mail;
    }

    public boolean saveNotice(String userId, String content) {
        MailClient client = null;
        boolean ret = false;
        try {
            client = pool.borrowObject();
            ret = client.getClient().saveNotice(userId,
                    content);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    public List<Notice> getNotice(String userId, int beginCnt, int count) {
        MailClient client = null;
        List<Notice> ret = null;
        try {
            client = pool.borrowObject();
            ret = client.getClient().getNotice(userId,
                    beginCnt,count);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally {
            if (null != client) {
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
