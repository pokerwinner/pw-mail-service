/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.tiantian.mail.thrift.mail;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2016-04-12")
public class MailReward implements org.apache.thrift.TBase<MailReward, MailReward._Fields>, java.io.Serializable, Cloneable, Comparable<MailReward> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("MailReward");

  private static final org.apache.thrift.protocol.TField MAIL_REWARD_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("mailRewardId", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField MAIL_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("mailId", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField REWARD_CODE_FIELD_DESC = new org.apache.thrift.protocol.TField("rewardCode", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField REWARD_NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("rewardName", org.apache.thrift.protocol.TType.STRING, (short)4);
  private static final org.apache.thrift.protocol.TField REWARD_NUMBER_FIELD_DESC = new org.apache.thrift.protocol.TField("rewardNumber", org.apache.thrift.protocol.TType.I64, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new MailRewardStandardSchemeFactory());
    schemes.put(TupleScheme.class, new MailRewardTupleSchemeFactory());
  }

  public String mailRewardId; // required
  public String mailId; // required
  public String rewardCode; // required
  public String rewardName; // required
  public long rewardNumber; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    MAIL_REWARD_ID((short)1, "mailRewardId"),
    MAIL_ID((short)2, "mailId"),
    REWARD_CODE((short)3, "rewardCode"),
    REWARD_NAME((short)4, "rewardName"),
    REWARD_NUMBER((short)5, "rewardNumber");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // MAIL_REWARD_ID
          return MAIL_REWARD_ID;
        case 2: // MAIL_ID
          return MAIL_ID;
        case 3: // REWARD_CODE
          return REWARD_CODE;
        case 4: // REWARD_NAME
          return REWARD_NAME;
        case 5: // REWARD_NUMBER
          return REWARD_NUMBER;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __REWARDNUMBER_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.MAIL_REWARD_ID, new org.apache.thrift.meta_data.FieldMetaData("mailRewardId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.MAIL_ID, new org.apache.thrift.meta_data.FieldMetaData("mailId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.REWARD_CODE, new org.apache.thrift.meta_data.FieldMetaData("rewardCode", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.REWARD_NAME, new org.apache.thrift.meta_data.FieldMetaData("rewardName", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.REWARD_NUMBER, new org.apache.thrift.meta_data.FieldMetaData("rewardNumber", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(MailReward.class, metaDataMap);
  }

  public MailReward() {
  }

  public MailReward(
    String mailRewardId,
    String mailId,
    String rewardCode,
    String rewardName,
    long rewardNumber)
  {
    this();
    this.mailRewardId = mailRewardId;
    this.mailId = mailId;
    this.rewardCode = rewardCode;
    this.rewardName = rewardName;
    this.rewardNumber = rewardNumber;
    setRewardNumberIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public MailReward(MailReward other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetMailRewardId()) {
      this.mailRewardId = other.mailRewardId;
    }
    if (other.isSetMailId()) {
      this.mailId = other.mailId;
    }
    if (other.isSetRewardCode()) {
      this.rewardCode = other.rewardCode;
    }
    if (other.isSetRewardName()) {
      this.rewardName = other.rewardName;
    }
    this.rewardNumber = other.rewardNumber;
  }

  public MailReward deepCopy() {
    return new MailReward(this);
  }

  @Override
  public void clear() {
    this.mailRewardId = null;
    this.mailId = null;
    this.rewardCode = null;
    this.rewardName = null;
    setRewardNumberIsSet(false);
    this.rewardNumber = 0;
  }

  public String getMailRewardId() {
    return this.mailRewardId;
  }

  public MailReward setMailRewardId(String mailRewardId) {
    this.mailRewardId = mailRewardId;
    return this;
  }

  public void unsetMailRewardId() {
    this.mailRewardId = null;
  }

  /** Returns true if field mailRewardId is set (has been assigned a value) and false otherwise */
  public boolean isSetMailRewardId() {
    return this.mailRewardId != null;
  }

  public void setMailRewardIdIsSet(boolean value) {
    if (!value) {
      this.mailRewardId = null;
    }
  }

  public String getMailId() {
    return this.mailId;
  }

  public MailReward setMailId(String mailId) {
    this.mailId = mailId;
    return this;
  }

  public void unsetMailId() {
    this.mailId = null;
  }

  /** Returns true if field mailId is set (has been assigned a value) and false otherwise */
  public boolean isSetMailId() {
    return this.mailId != null;
  }

  public void setMailIdIsSet(boolean value) {
    if (!value) {
      this.mailId = null;
    }
  }

  public String getRewardCode() {
    return this.rewardCode;
  }

  public MailReward setRewardCode(String rewardCode) {
    this.rewardCode = rewardCode;
    return this;
  }

  public void unsetRewardCode() {
    this.rewardCode = null;
  }

  /** Returns true if field rewardCode is set (has been assigned a value) and false otherwise */
  public boolean isSetRewardCode() {
    return this.rewardCode != null;
  }

  public void setRewardCodeIsSet(boolean value) {
    if (!value) {
      this.rewardCode = null;
    }
  }

  public String getRewardName() {
    return this.rewardName;
  }

  public MailReward setRewardName(String rewardName) {
    this.rewardName = rewardName;
    return this;
  }

  public void unsetRewardName() {
    this.rewardName = null;
  }

  /** Returns true if field rewardName is set (has been assigned a value) and false otherwise */
  public boolean isSetRewardName() {
    return this.rewardName != null;
  }

  public void setRewardNameIsSet(boolean value) {
    if (!value) {
      this.rewardName = null;
    }
  }

  public long getRewardNumber() {
    return this.rewardNumber;
  }

  public MailReward setRewardNumber(long rewardNumber) {
    this.rewardNumber = rewardNumber;
    setRewardNumberIsSet(true);
    return this;
  }

  public void unsetRewardNumber() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __REWARDNUMBER_ISSET_ID);
  }

  /** Returns true if field rewardNumber is set (has been assigned a value) and false otherwise */
  public boolean isSetRewardNumber() {
    return EncodingUtils.testBit(__isset_bitfield, __REWARDNUMBER_ISSET_ID);
  }

  public void setRewardNumberIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __REWARDNUMBER_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case MAIL_REWARD_ID:
      if (value == null) {
        unsetMailRewardId();
      } else {
        setMailRewardId((String)value);
      }
      break;

    case MAIL_ID:
      if (value == null) {
        unsetMailId();
      } else {
        setMailId((String)value);
      }
      break;

    case REWARD_CODE:
      if (value == null) {
        unsetRewardCode();
      } else {
        setRewardCode((String)value);
      }
      break;

    case REWARD_NAME:
      if (value == null) {
        unsetRewardName();
      } else {
        setRewardName((String)value);
      }
      break;

    case REWARD_NUMBER:
      if (value == null) {
        unsetRewardNumber();
      } else {
        setRewardNumber((Long)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case MAIL_REWARD_ID:
      return getMailRewardId();

    case MAIL_ID:
      return getMailId();

    case REWARD_CODE:
      return getRewardCode();

    case REWARD_NAME:
      return getRewardName();

    case REWARD_NUMBER:
      return getRewardNumber();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case MAIL_REWARD_ID:
      return isSetMailRewardId();
    case MAIL_ID:
      return isSetMailId();
    case REWARD_CODE:
      return isSetRewardCode();
    case REWARD_NAME:
      return isSetRewardName();
    case REWARD_NUMBER:
      return isSetRewardNumber();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof MailReward)
      return this.equals((MailReward)that);
    return false;
  }

  public boolean equals(MailReward that) {
    if (that == null)
      return false;

    boolean this_present_mailRewardId = true && this.isSetMailRewardId();
    boolean that_present_mailRewardId = true && that.isSetMailRewardId();
    if (this_present_mailRewardId || that_present_mailRewardId) {
      if (!(this_present_mailRewardId && that_present_mailRewardId))
        return false;
      if (!this.mailRewardId.equals(that.mailRewardId))
        return false;
    }

    boolean this_present_mailId = true && this.isSetMailId();
    boolean that_present_mailId = true && that.isSetMailId();
    if (this_present_mailId || that_present_mailId) {
      if (!(this_present_mailId && that_present_mailId))
        return false;
      if (!this.mailId.equals(that.mailId))
        return false;
    }

    boolean this_present_rewardCode = true && this.isSetRewardCode();
    boolean that_present_rewardCode = true && that.isSetRewardCode();
    if (this_present_rewardCode || that_present_rewardCode) {
      if (!(this_present_rewardCode && that_present_rewardCode))
        return false;
      if (!this.rewardCode.equals(that.rewardCode))
        return false;
    }

    boolean this_present_rewardName = true && this.isSetRewardName();
    boolean that_present_rewardName = true && that.isSetRewardName();
    if (this_present_rewardName || that_present_rewardName) {
      if (!(this_present_rewardName && that_present_rewardName))
        return false;
      if (!this.rewardName.equals(that.rewardName))
        return false;
    }

    boolean this_present_rewardNumber = true;
    boolean that_present_rewardNumber = true;
    if (this_present_rewardNumber || that_present_rewardNumber) {
      if (!(this_present_rewardNumber && that_present_rewardNumber))
        return false;
      if (this.rewardNumber != that.rewardNumber)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_mailRewardId = true && (isSetMailRewardId());
    list.add(present_mailRewardId);
    if (present_mailRewardId)
      list.add(mailRewardId);

    boolean present_mailId = true && (isSetMailId());
    list.add(present_mailId);
    if (present_mailId)
      list.add(mailId);

    boolean present_rewardCode = true && (isSetRewardCode());
    list.add(present_rewardCode);
    if (present_rewardCode)
      list.add(rewardCode);

    boolean present_rewardName = true && (isSetRewardName());
    list.add(present_rewardName);
    if (present_rewardName)
      list.add(rewardName);

    boolean present_rewardNumber = true;
    list.add(present_rewardNumber);
    if (present_rewardNumber)
      list.add(rewardNumber);

    return list.hashCode();
  }

  @Override
  public int compareTo(MailReward other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetMailRewardId()).compareTo(other.isSetMailRewardId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMailRewardId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.mailRewardId, other.mailRewardId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetMailId()).compareTo(other.isSetMailId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetMailId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.mailId, other.mailId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetRewardCode()).compareTo(other.isSetRewardCode());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRewardCode()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.rewardCode, other.rewardCode);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetRewardName()).compareTo(other.isSetRewardName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRewardName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.rewardName, other.rewardName);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetRewardNumber()).compareTo(other.isSetRewardNumber());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRewardNumber()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.rewardNumber, other.rewardNumber);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("MailReward(");
    boolean first = true;

    sb.append("mailRewardId:");
    if (this.mailRewardId == null) {
      sb.append("null");
    } else {
      sb.append(this.mailRewardId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("mailId:");
    if (this.mailId == null) {
      sb.append("null");
    } else {
      sb.append(this.mailId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("rewardCode:");
    if (this.rewardCode == null) {
      sb.append("null");
    } else {
      sb.append(this.rewardCode);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("rewardName:");
    if (this.rewardName == null) {
      sb.append("null");
    } else {
      sb.append(this.rewardName);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("rewardNumber:");
    sb.append(this.rewardNumber);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class MailRewardStandardSchemeFactory implements SchemeFactory {
    public MailRewardStandardScheme getScheme() {
      return new MailRewardStandardScheme();
    }
  }

  private static class MailRewardStandardScheme extends StandardScheme<MailReward> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, MailReward struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // MAIL_REWARD_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.mailRewardId = iprot.readString();
              struct.setMailRewardIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // MAIL_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.mailId = iprot.readString();
              struct.setMailIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // REWARD_CODE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.rewardCode = iprot.readString();
              struct.setRewardCodeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // REWARD_NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.rewardName = iprot.readString();
              struct.setRewardNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // REWARD_NUMBER
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.rewardNumber = iprot.readI64();
              struct.setRewardNumberIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, MailReward struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.mailRewardId != null) {
        oprot.writeFieldBegin(MAIL_REWARD_ID_FIELD_DESC);
        oprot.writeString(struct.mailRewardId);
        oprot.writeFieldEnd();
      }
      if (struct.mailId != null) {
        oprot.writeFieldBegin(MAIL_ID_FIELD_DESC);
        oprot.writeString(struct.mailId);
        oprot.writeFieldEnd();
      }
      if (struct.rewardCode != null) {
        oprot.writeFieldBegin(REWARD_CODE_FIELD_DESC);
        oprot.writeString(struct.rewardCode);
        oprot.writeFieldEnd();
      }
      if (struct.rewardName != null) {
        oprot.writeFieldBegin(REWARD_NAME_FIELD_DESC);
        oprot.writeString(struct.rewardName);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(REWARD_NUMBER_FIELD_DESC);
      oprot.writeI64(struct.rewardNumber);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class MailRewardTupleSchemeFactory implements SchemeFactory {
    public MailRewardTupleScheme getScheme() {
      return new MailRewardTupleScheme();
    }
  }

  private static class MailRewardTupleScheme extends TupleScheme<MailReward> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, MailReward struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetMailRewardId()) {
        optionals.set(0);
      }
      if (struct.isSetMailId()) {
        optionals.set(1);
      }
      if (struct.isSetRewardCode()) {
        optionals.set(2);
      }
      if (struct.isSetRewardName()) {
        optionals.set(3);
      }
      if (struct.isSetRewardNumber()) {
        optionals.set(4);
      }
      oprot.writeBitSet(optionals, 5);
      if (struct.isSetMailRewardId()) {
        oprot.writeString(struct.mailRewardId);
      }
      if (struct.isSetMailId()) {
        oprot.writeString(struct.mailId);
      }
      if (struct.isSetRewardCode()) {
        oprot.writeString(struct.rewardCode);
      }
      if (struct.isSetRewardName()) {
        oprot.writeString(struct.rewardName);
      }
      if (struct.isSetRewardNumber()) {
        oprot.writeI64(struct.rewardNumber);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, MailReward struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(5);
      if (incoming.get(0)) {
        struct.mailRewardId = iprot.readString();
        struct.setMailRewardIdIsSet(true);
      }
      if (incoming.get(1)) {
        struct.mailId = iprot.readString();
        struct.setMailIdIsSet(true);
      }
      if (incoming.get(2)) {
        struct.rewardCode = iprot.readString();
        struct.setRewardCodeIsSet(true);
      }
      if (incoming.get(3)) {
        struct.rewardName = iprot.readString();
        struct.setRewardNameIsSet(true);
      }
      if (incoming.get(4)) {
        struct.rewardNumber = iprot.readI64();
        struct.setRewardNumberIsSet(true);
      }
    }
  }

}

